﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Xml;
using System.IO;

namespace Services.TestDataAccess
{
    public class DataAccess
    {
         public IDictionary<string,string> GetDataFromXML(string testName)
        {
            var testData = Properties.Resources.testData;
            IDictionary<string, string> paramList = new Dictionary<string, string>(){ };
            
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(testData);

            XmlNode testNode = xmlDoc.DocumentElement.SelectSingleNode(testName);

            foreach (XmlNode node in testNode.ChildNodes)
            {
                paramList.Add(node.Name, node.InnerText);
            }
            
            return paramList;
        }

         

         
    }
}
