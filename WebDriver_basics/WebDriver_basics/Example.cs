﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace WebDriver_basics
{
    [TestFixture]
    public class Example
    {

        [Test, Category("Example")]
        public void Login_page()
        {
            var jquery = "return $('h1.firstHeading:contains(\"Create account\")').get(0)";
            var headerCssSelector = "h1.firstHeading";
            IWebDriver chromeBrowser = new ChromeDriver();
            chromeBrowser.Navigate().GoToUrl("https://en.wikipedia.org/w/index.php?title=Special:CreateAccount&returnto=Main+Page");
            IJavaScriptExecutor js = (IJavaScriptExecutor)chromeBrowser;

            IWebElement returnedHeaderTextFromJquery = (IWebElement)js.ExecuteScript(jquery);
            var headerTextFromjQuery = returnedHeaderTextFromJquery.Text;
            
            var headerTextFromCssSelector = chromeBrowser.FindElement(By.CssSelector(headerCssSelector));
            Assert.True(headerTextFromCssSelector.Displayed, "Header Text is being displayed");
            Assert.True(headerTextFromCssSelector.Text.Equals("Create account"), "Header Text is showing correct value");

            chromeBrowser.Quit();

            
        }


    }
}
