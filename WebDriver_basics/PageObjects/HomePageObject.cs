﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjects
{
    public class HomePageObject
    {
        public const string SearchByHotelOrCityInputCSSLocator = "div.locationlisthotels input.select2-focusser";//"div.select-18-drop-active div.select2-search input.select2-input";

        public const string CheckInDateCSSLocator = "input[Name=\"checkin\"]";
        public const string CheckOutDateCSSLocator = "input[Name=\"checkout\"]";

        public const string SelectActiveDateCSSLocator = "div[style*=\"display: block\"].datepicker.dropdown-menu tr td.day.active";
        public const string SelectParticularDateFromPickerCSSLocator = "div[style*=\"display: block\"].datepicker.dropdown-menu tr td:not(.disabled.old.day)" +
                                                             ":contains(\"31\")";

        public const string SearchButtonCssLocator = "div#hotels button[type=submit]";

        public const string SelectHotelDetailByRowNumberCssLocator = "table.table-striped tr:nth-child({0}) button:contains(Details)";

        public const string HotelPriceOptionsCssSelector = "h2.book_price strong";

    }
}
