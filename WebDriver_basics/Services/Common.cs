﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Services
{
    public class Common
    {
        private IWebDriver driver = null;
        private IJavaScriptExecutor js;
        private const string UserDropDownInNavBarCssLocator = "nav.navbar ul.navbar-right.user_menu a.dropdown-toggle:has(i.icon_set_1_icon-70)";
        private const string GetLoggedInUserFromNavBarCssLocator = UserDropDownInNavBarCssLocator + ":contains(\"{0}\")";
        private const string LogoutButtonFromUserDropdownCssLocator = "li.open ul.dropdown-menu a:contains(Logout)";
        

        public Common(IWebDriver webDriver)
        {
            this.driver = webDriver;
            this.js = (IJavaScriptExecutor) driver;
        }

        public void ScrollIntoView(IWebElement element)
        {
            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(500);
        }
        public void WaitUntilPageLoad()
        {
            new WebDriverWait(driver, TimeSpan.FromMilliseconds(5000)).Until
            (
                d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete")
            );
        }

        public void WaitForCondition(Func<bool> f, int milliSec = 0)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(milliSec));

            try
            {
                wait.Until(d =>
                {
                    try
                    {
                        return f();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public bool IsUserLoggedIn(string userFirstName)
        {
            try
            {
                var jquery = string.Format("return $('{0}').get(0)", string.Format(GetLoggedInUserFromNavBarCssLocator, userFirstName));
                var elem = (IWebElement)js.ExecuteScript(jquery);

                return elem.Text.ToLower().Contains(userFirstName.ToLower()) ? true : false;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
                //throw;
            }
        }

        public void LogOut()
        {
            var jquery1 = string.Format("return $('{0}').get(0)", UserDropDownInNavBarCssLocator);
            var jquery2 = string.Format("return $('{0}').get(0)", LogoutButtonFromUserDropdownCssLocator);

            var userDropdownIcon = (IWebElement) js.ExecuteScript(jquery1);
            userDropdownIcon.Click();

            var logoutOption = (IWebElement) js.ExecuteScript(jquery2);
            logoutOption.Click();
        }
    }
}
