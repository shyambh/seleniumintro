﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace PageObjects
{
    public class UserProfilePageObject
    {
        public const string HotelBookingsRowCssLocator = "div#bookings div.row";
        public const string HotelIconCssLocator = "li[data-title=\"hotels\"]";
        public const string WelcomeMessageCssLocator = "h3.RTL";//"h3:contains(\"Hi, Johny Smith\")";
    }
}
