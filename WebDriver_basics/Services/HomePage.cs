﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using PageObjects;
using Services;

namespace Services
{
    public class HomePage
    {
        public IWebDriver driver;
        public IJavaScriptExecutor js;
        public Common _commonFunctionalities;
        public HomePage(IWebDriver webDriver)
        {
            this.driver = webDriver;
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            this._commonFunctionalities = new Common(driver);
        }

        public void EnterHotelOrCityName(string hotelOrCityName)
        {
            driver.FindElement(By.CssSelector(HomePageObject.SearchByHotelOrCityInputCSSLocator)).SendKeys(hotelOrCityName);
            driver.FindElement(By.CssSelector("div.select2-result-label span")).Click();
        }

        public bool IsDatePickerDisplayed()
        {
            try
            {
                driver.FindElement(By.CssSelector(HomePageObject.SelectActiveDateCSSLocator));
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }


        public void EnterCheckInDate(bool selectActiveDate = true)
        {
            if (selectActiveDate)
            {
                if (!IsDatePickerDisplayed())
                    driver.FindElement(By.CssSelector(HomePageObject.CheckInDateCSSLocator)).Click();

                driver.FindElement(By.CssSelector(HomePageObject.SelectActiveDateCSSLocator)).Click();
            }
            else
            {
                // Code for dynamically pick other dates from calendar
            }
                
        }


        public void EnterCheckOutDate(bool selectActiveDate = true)
        {
            if (selectActiveDate)
            {
                if(!IsDatePickerDisplayed())
                    driver.FindElement(By.CssSelector(HomePageObject.CheckOutDateCSSLocator)).Click();

                driver.FindElement(By.CssSelector(HomePageObject.SelectActiveDateCSSLocator)).Click();
            }
            else
            {
                // Code for dynamically pick other dates from calendar
            }

        }

        public void ClickSearchButton()
        {
            driver.FindElement(By.CssSelector(HomePageObject.SearchButtonCssLocator)).Click();
        }

        public void SelectHotelDetailByRowNumber(int row = 1)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var jquery = string.Format("return $('{0}').get(0)", string.Format(HomePageObject.SelectHotelDetailByRowNumberCssLocator, row));
            IWebElement elem = (IWebElement)js.ExecuteScript(jquery);
            elem.Click();
        }

        public List<int> GetHotelPriceOptions()
        {
            var elementListOfPriceOptions = driver.FindElements(By.CssSelector(HomePageObject.HotelPriceOptionsCssSelector));
            List<int> listOfPriceOptions = new List<int>();

            foreach (var element in elementListOfPriceOptions)
            {
                _commonFunctionalities.ScrollIntoView(element);
                listOfPriceOptions.Add((Int32.Parse(element.Text.TrimStart('$'))));
            }



            return listOfPriceOptions;
        }


    }
}
