﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjects
{
    public class AdminLoginPageObjects
    {
        public const string EmailInputFieldCssLocator = "input[name=email][type=text]";
        public const string PasswordInputFieldCssLocator = "input[name=password][type=password]";
        public const string LoginButtonCssSelector = "button[type=submit]";
        public const string DashboardCssLocator = "a.dash";
        public const string BookingButtonXPath = "//button//div[contains(text(), \"Bookings\")]";
        public const string BookingManageMentHeaderCssLocator = "div:contains(\"Booking Management\").panel-heading";
        public const string BookingCountCssLocator = "table tr";
    }
}
