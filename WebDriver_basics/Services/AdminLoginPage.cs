﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using PageObjects;

namespace Services
{
    public class AdminLoginPage
    {
        private IWebDriver driver;
        private AdminLoginPageObjects adminLoginPageObjects;
        private Common _commonFunctionalities;

        public AdminLoginPage(IWebDriver webDriver)
        {
            this.driver = webDriver;
            this.adminLoginPageObjects = new AdminLoginPageObjects();
            this._commonFunctionalities = new Common(webDriver);

        }
        public void EnterAdminUserName(string username)
        {
            driver.FindElement(By.CssSelector(AdminLoginPageObjects.EmailInputFieldCssLocator)).SendKeys(username);
        }

        public void EnterAdminPassword(string password)
        {
            driver.FindElement(By.CssSelector(AdminLoginPageObjects.PasswordInputFieldCssLocator)).SendKeys(password);
        }

        public bool IsDashboardShown()
        {
            try
            {
                return driver.FindElement(By.CssSelector(AdminLoginPageObjects.DashboardCssLocator)).Displayed;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            
        }

        public void ClickLoginButton()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(6000);
            driver.FindElement(By.CssSelector(AdminLoginPageObjects.LoginButtonCssSelector)).Click();
            _commonFunctionalities.WaitUntilPageLoad();
            _commonFunctionalities.WaitForCondition(IsDashboardShown);
        }

        public bool IsBookingManagementHeaderPresent()
        {
            try
            {
                driver.FindElement(By.CssSelector(AdminLoginPageObjects.BookingManageMentHeaderCssLocator));
                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }


        public void ClickBookingButton()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(5000);
            driver.FindElement(By.XPath(AdminLoginPageObjects.BookingButtonXPath)).Click();
        }

        public int GetBookingCount()
        {
            return driver.FindElements(By.CssSelector(AdminLoginPageObjects.BookingCountCssLocator)).Count;
        }
    }
}
