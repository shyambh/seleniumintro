﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjects
{
    public class LoginPageObjects
    {
        private IWebDriver driver;

        public LoginPageObjects(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public const string EmailInputFieldCssLocator = "div.form-group input[type=Email]";
        public const string PasswordInputFieldCssLocator = "div.form-group input[type=Password]";
        public const string LoginButtonCssLocator = "form button.loginbtn";
        public const string InvalidUserMessageCssLocator = "div.alert";


        [FindsBy(How = How.CssSelector, Using = EmailInputFieldCssLocator)]
        public IWebElement emailInputField;

        [FindsBy(How = How.CssSelector, Using = PasswordInputFieldCssLocator)]
        public IWebElement passwordInputField;

        [FindsBy(How = How.CssSelector, Using = LoginButtonCssLocator)]
        public IWebElement loginButton;

    }
}
