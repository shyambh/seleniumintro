﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjects;
using Services;

namespace Services
{
    public class UserProfilePage
    { 
        public IWebDriver driver;
        UserProfilePageObject _userProfilePageObjects;
        public Common _commonFunctionalities;
         

        public UserProfilePage(IWebDriver webDriver)
        {
            this.driver = webDriver;
            _userProfilePageObjects = new UserProfilePageObject();
            _commonFunctionalities = new Common(driver);
        }

        public Common GetCommonFunctionalities()
        {
            return _commonFunctionalities;
        }

        public int CountOfHotelBookings()
        {
            return driver.FindElements(By.CssSelector(UserProfilePageObject.HotelBookingsRowCssLocator)).Count;
        }


        public HomePage ClickOnHotelsIconAndNavigateToHomePage()
        {
            driver.FindElement(By.CssSelector(UserProfilePageObject.HotelIconCssLocator)).Click();
            _commonFunctionalities.WaitUntilPageLoad();
            var _homePage =  new HomePage(driver);
            return _homePage;
        }
        public string GetWelcomeMessage()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(5000);

            var text = driver.FindElement(By.CssSelector(UserProfilePageObject.WelcomeMessageCssLocator)).Text;
            return text;
        }


    }
}
