﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;

namespace Services
{
    public class PhpTravelsLoginPage 
    {
        public IWebDriver driver;
        LoginPageObjects _phpTravelsPageObject;
        public Common _commonFunctionalities;

        public PhpTravelsLoginPage(IWebDriver webdriver)
        {
            _phpTravelsPageObject = new LoginPageObjects(webdriver);
            this.driver = webdriver;
            _commonFunctionalities = new Common(driver);
        }

        public Common GetCommonFunctionalties()
        {
            return _commonFunctionalities;
        }

        

        public void EnterUsername(string username)
        {
            _phpTravelsPageObject.emailInputField.Clear();
            Console.WriteLine("Entering Username as : {0}", username);
            _phpTravelsPageObject.emailInputField.SendKeys(username);
        }

        public void EnterPassword(string password)
        {
            _phpTravelsPageObject.passwordInputField.Clear();
            _phpTravelsPageObject.passwordInputField.SendKeys(password);
        }

        public void ClickLoginButton()
        {
            _phpTravelsPageObject.loginButton.Click();
        }

        public UserProfilePage ClickLoginButtonToNavigateToUserProfilePage()
        {
            ClickLoginButton();
            UserProfilePage _userProfilePage = new UserProfilePage(driver);
            _commonFunctionalities.WaitUntilPageLoad();

            _commonFunctionalities.WaitForCondition(() =>
                {
                    return _userProfilePage.GetWelcomeMessage() == null ? false : true;
                });

            return _userProfilePage;
        }

        public bool IsInvalidUserNameAlertMessagePresent()
        {
            try
            {
                var element = driver.FindElement(By.CssSelector(LoginPageObjects.InvalidUserMessageCssLocator));
                return element.Displayed;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }



        public string GetInvalidUsernameAlertTextMessage()
        {
            if (IsInvalidUserNameAlertMessagePresent())
                return driver.FindElement(By.CssSelector(LoginPageObjects.InvalidUserMessageCssLocator)).Text;
            else
                return null;
        }

        


    }
}
