﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Services.TestDataAccess;
using System.Diagnostics;
using System.Collections;
using System.Linq;
using Services;
using OpenQA.Selenium.Support.UI;


namespace WebDriver_basics
{
    [TestFixture]
    public class TestSuite
    {
        public DataAccess _dataAccess = new DataAccess();
        public IWebDriver _chromeBrowser = null;
        public PhpTravelsLoginPage _phpTravelsLoginPage;
        public AdminLoginPage _adminLoginPage;

        [OneTimeSetUp]
        public void ClassInit()
        {
            _chromeBrowser = new ChromeDriver();
            
            _phpTravelsLoginPage = new PhpTravelsLoginPage(_chromeBrowser);
            _adminLoginPage = new AdminLoginPage(_chromeBrowser);
        }


        [OneTimeTearDown]
        public void TestCleanUp()
        {
            _chromeBrowser.Quit();
        }

        [Test, Category("Example")]
        public void Wikipedia_Login_page()
        {
            var methodName = new StackFrame(true).GetMethod().Name;

            var jquery = "return $('h1.firstHeading:contains(\"Create account\")').get(0)";
            var headerCssSelector = "h1.firstHeading";
            //IWebDriver chromeBrowser = new ChromeDriver();
            _chromeBrowser.Navigate().GoToUrl("https://en.wikipedia.org/w/index.php?title=Special:CreateAccount&returnto=Main+Page");
            IJavaScriptExecutor js = (IJavaScriptExecutor)_chromeBrowser;

            IWebElement returnedHeaderTextFromJquery = (IWebElement)js.ExecuteScript(jquery);
            var headerTextFromjQuery = returnedHeaderTextFromJquery.Text;
            
            var headerTextFromCssSelector = _chromeBrowser.FindElement(By.CssSelector(headerCssSelector));
            Assert.True(headerTextFromCssSelector.Displayed, "Header Text is being displayed");
            Assert.True(headerTextFromCssSelector.Text.Equals("Create account"), "Header Text is showing correct value");
        }

        [Test, Category("Login")]
        public void Verify_invalid_logins_for_php_travels()
        {
            var testName = new StackFrame(true).GetMethod().Name;
            var testData = _dataAccess.GetDataFromXML(testName);
            
            var invalidUserNames = testData["InvalidUsernames"].Split(';').ToList();
            var invalidPasswords = testData["InvalidPasswords"].Split(';').ToList();

            _chromeBrowser.Navigate().GoToUrl(testData["UrlToTest"]);

            int i;
            for(i = 0; i < invalidUserNames.Count; i++)
            {
                _phpTravelsLoginPage.EnterUsername(invalidUserNames[i]);
                _phpTravelsLoginPage.EnterPassword(invalidPasswords[i]);
                _phpTravelsLoginPage.ClickLoginButton();
                _phpTravelsLoginPage.GetCommonFunctionalties().WaitForCondition(_phpTravelsLoginPage.IsInvalidUserNameAlertMessagePresent, 2000);

                Assert.IsTrue(_phpTravelsLoginPage.IsInvalidUserNameAlertMessagePresent(), 
                    "Is alert message stating invalid username or password displayed ?");
                
                Assert.AreEqual(_phpTravelsLoginPage.GetInvalidUsernameAlertTextMessage().Trim(), "Invalid Email or Password");
            }

        }

        [Test, Category("Login")]
        public void Verify_valid_login_for_demo_user()
        {
            var testName = new StackFrame(true).GetMethod().Name;
            var testData = _dataAccess.GetDataFromXML(testName);

            var validDemoUsername = testData["ValidDemoUsername"];
            var validDemoPassword = testData["ValidDemoPassword"];

            _chromeBrowser.Navigate().GoToUrl(testData["UrlToTest"]);
            
            var isAlreadyLoggedIn = _phpTravelsLoginPage.GetCommonFunctionalties().IsUserLoggedIn("Johny");
            if (isAlreadyLoggedIn)
                _phpTravelsLoginPage.GetCommonFunctionalties().LogOut();
            _phpTravelsLoginPage.EnterUsername(validDemoUsername);
            _phpTravelsLoginPage.EnterPassword(validDemoPassword);
            var _userProfilePage = _phpTravelsLoginPage.ClickLoginButtonToNavigateToUserProfilePage();

            Assert.IsNotNull(_userProfilePage);
            var welcomeText = _userProfilePage.GetWelcomeMessage();

            Assert.AreEqual(welcomeText, "Hi, Johny Smith");
        }

        [Test, Category("HotelReservation")]
        public void Verify_user_can_successfully_make_a_hotel_reservation()
        {
            var testName = new StackFrame(true).GetMethod().Name;
            var testData = _dataAccess.GetDataFromXML(testName);

            var validDemoUsername = testData["ValidDemoUsername"];
            var validDemoPassword = testData["ValidDemoPassword"];

            try
            {
                _chromeBrowser.Navigate().GoToUrl(testData["UrlToTest"]);

                _phpTravelsLoginPage.EnterUsername(validDemoUsername);
                _phpTravelsLoginPage.EnterPassword(validDemoPassword);
                var _userProfilePage = _phpTravelsLoginPage.ClickLoginButtonToNavigateToUserProfilePage();

                _chromeBrowser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
                var countOfBookings = _userProfilePage.CountOfHotelBookings();
                var _homePage = _userProfilePage.ClickOnHotelsIconAndNavigateToHomePage();
                
                _homePage.EnterHotelOrCityName("Kathmandu");
                _homePage.EnterCheckInDate();
                _homePage.EnterCheckOutDate();
                _homePage.ClickSearchButton();

                _homePage.SelectHotelDetailByRowNumber();

                var priceOptions = _homePage.GetHotelPriceOptions();
                var priceFromUI = priceOptions;

                priceOptions.Sort();

                for (int i = 0; i < priceFromUI.Count; i++)
                {
                    Assert.AreEqual(priceOptions[i], priceFromUI[i]);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
            }
        }

        [Test, Category("AdminTest")]
        public void Verify_admin_user_can_navigate_to_admin_page()
        {
            _chromeBrowser.Navigate().GoToUrl("https://www.phptravels.net/admin");
            _adminLoginPage.EnterAdminUserName("admin@phptravels.com");
            _adminLoginPage.EnterAdminPassword("demoadmin");
            _adminLoginPage.ClickLoginButton();

            Assert.True(_adminLoginPage.IsDashboardShown());

            _adminLoginPage.ClickBookingButton();
            var count = _adminLoginPage.GetBookingCount();
            Assert.Greater(count, 0);
        }

        [Test, Category("ExplicitWait")]
        public void Verify_wait_for_loading()
        {
            _chromeBrowser.Navigate().GoToUrl("http://the-internet.herokuapp.com/dynamic_loading/2");
            _chromeBrowser.FindElement(By.CssSelector("div#start button")).Click();

            WebDriverWait waitUntilLoading = new WebDriverWait(_chromeBrowser, TimeSpan.FromSeconds(10));
            waitUntilLoading.Until(d =>
                {
                    var loadingVisible = _chromeBrowser.FindElement(By.CssSelector("div#loading:not([style=\"display: none; \"])"));

                    try
                    {
                        if (!loadingVisible.Displayed)
                            return true;
                        else
                            return false;
                    }
                    catch (Exception e) 
                    {
                        Console.WriteLine(e.StackTrace);
                        return false;
                    }
                });

            var text = _chromeBrowser.FindElement(By.CssSelector("div#finish h4")).Text;
            Assert.True(String.Equals(text, "Hello World!"));
        }



        public void LoginToApplication(string testName)
        {
            var testData = _dataAccess.GetDataFromXML(testName);

        }



       


    }
}
